<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

Route::get('/', [App\Http\Controllers\WelcomeController::class, 'index'])->name('fetching_users');
Route::get('/users-with-pagination', [App\Http\Controllers\WelcomeController::class, 'usersWithPagination'])->name('users_with_pagination')->middleware('auth');
Route::get('/users-with-search', [App\Http\Controllers\WelcomeController::class, 'usersWithSearch'])->name('users_with_search');
