<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class fetchUsersEvery8Hours extends Command {

  protected $signature = 'fetch:users';

  protected $description = 'fetch users form end point every 8 hours';

  public function __construct() {
    parent::__construct();
  }

  public function handle() {

    //use
    // $data = file_get_contents('https://60e1b5fc5a5596001730f1d6.mockapi.io/api/v1/users/users_1', true);

    // Or

    // fetching data user_1 form first link
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, 'https://60e1b5fc5a5596001730f1d6.mockapi.io/api/v1/users/users_1');
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    $users_1 = curl_exec($c);

    // fetching data user_1 form scend link
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, 'https://60e1b5fc5a5596001730f1d6.mockapi.io/api/v1/users/user_2');
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    $users_2 = curl_exec($c);

    // decode string => Arr
    $arr_users_1 = json_decode($users_1, true);
    $arr_users_2 = json_decode($users_2, true);

    // change diff keys
    foreach ($arr_users_2 as &$arr) {
      $arr["firstName"] = $arr['fName'];
      unset($arr['fName']);
      $arr['lastName'] = $arr['lName'];
      unset($arr['lName']);
      $arr['avatar'] = $arr['picture'];
      unset($arr['picture']);
    }
    // dd($arr_users_2);

    // merge users 2 array 1 & 2
    $total_users = array_merge($arr_users_1, $arr_users_2);

    // validate request arrays to save
    $validator = Validator::make($total_users, [
      '*.email'     => 'required|unique:users,email,',
      '*.firstName' => 'required',
      '*.lastName'  => 'required',
      '*.avatar'    => 'required',
    ]);

    // check validation
    if ($validator->fails()) {
      session('errors', $validator->errors());
    } else {
      // save data
      foreach ($total_users as $key => $user) {
        User::create([
          'email'     => $user['email'],
          'firstName' => $user['firstName'],
          'lastName'  => $user['lastName'],
          'avatar'    => $user['avatar'],
        ]);
      }

    }

  }

}
