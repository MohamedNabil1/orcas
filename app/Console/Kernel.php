<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

  protected $commands = [
    Commands\fetchUsersEvery8Hours::class,
  ];

  protected function schedule(Schedule $schedule) {
    $schedule->command('fetch:users')
      ->everyEightHours();
  }

  /**
   * Register the commands for the application.
   *
   * @return void
   */
  protected function commands() {
    $this->load(__DIR__ . '/Commands');

    require base_path('routes/console.php');
  }
}
