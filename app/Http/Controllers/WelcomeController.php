<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller {

  /******** index ************/
  public function index() {
    $users = DB::select('select * from users');
    return view('welcome', \compact('users'));
  }

  /******** users With Pagination ************/
  public function usersWithPagination() {

    $users = User::latest()->paginate(10);
    return view('users_with_pagination', \compact('users'));
  }

  /******** users With Search ************/
  public function usersWithSearch(Request $request) {
    $users = User::when(request('search'), function ($q) {
      return $q->where('firstName', 'like', '%' . request('search') . '%')
        ->orWhere('lastName', 'like', '%' . request('search') . '%')
        ->orWhere('email', 'like', '%' . request('search') . '%');
    })->latest()->paginate(10);

    return view('search', \compact('users'));
  }
}
