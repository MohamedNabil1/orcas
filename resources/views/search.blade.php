@extends('master')
@section('contect')

<form class="form-inline my-2 my-lg-0" action="{{route('users_with_search')}}"
  method="GET">
  <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search"
    name="search" value="{{ request()->search }}">
  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  <a class="btn btn-outline-success my-2 my-sm-0"
    href="{{route('users_with_search')}}">Back</a>
</form>
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($users as $index => $user)
    <tr>
      <th scope="row">{{$index + 1}}</th>
      <td>{{$user->firstName}}</td>
      <td>{{$user->lastName}}</td>
      <td>{{$user->email}}</td>
    </tr>
    @endforeach

  </tbody>
</table>

{{ $users->appends(request()->query())->links() }}


@endsection