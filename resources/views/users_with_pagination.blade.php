@extends('master')
@section('contect')


<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($users as $index => $user)
    <tr>
      <th scope="row">{{$index + 1}}</th>
      <td>{{$user->firstName}}</td>
      <td>{{$user->lastName}}</td>
      <td>{{$user->email}}</td>
    </tr>
    @endforeach

  </tbody>
</table>
{!! $users->links() !!}

@endsection